﻿

var pca = angular.module("myApp", ['ngRoute', 'ngTouch']);
pca.config(['$routeProvider',

    function ($routeProvider) {
        $routeProvider.
        when('/lang', {
            templateUrl: 'app/view/language.html',
            controller: 'langController',
            controllerAs: 'lang'
        }).

        when('/menu', {
            templateUrl: 'app/view/menu.html',
            controller: 'menuController',
            controllerAs: 'menu'

        }).

        when('/video', {
            templateUrl: 'app/view/video.html',
            controller: 'videoController',
            controllerAs: 'video'

            }).
          when('/video2', {
            templateUrl: 'app/view/video2.html',
            controller: 'videoController',
            controllerAs: 'video'

            }).



         when('/share', {
             templateUrl: 'app/view/share.html',
             controller: 'shareController',
             controllerAs: 'share'

         }).

        when('/contact', {
            templateUrl: 'app/view/contact.html',
            controller: 'contactController',
            controllerAs: 'contact'

        });

    }]);