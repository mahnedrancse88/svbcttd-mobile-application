﻿pca.controller('langController', ['$scope','$rootScope', '$location',
    function ($scope, $rootScope,$location) {

        var mobileWidth = $(window).width();
        var mobileWHeight = $(window).height();
       // mobileWHeight = mobileWHeight - 50;
        mobileWHeight = mobileWHeight + "px";

        $(".canal-logo-bg").css("height", mobileWHeight);
                $rootScope.isShowheader=false;

		

        $scope.languagePreference = function (lan) {

            $rootScope.preferenceLanguage = lan;
            $rootScope.isShowslidemenu = false;

            if ($rootScope.preferenceLanguage == 'english') {

                $rootScope.slidemenu0 = "Home";
                $rootScope.slidemenu1 = "Live Tv";
                $rootScope.slidemenu2 = "Movie";
                $rootScope.slidemenu3 = "Replay";
                $rootScope.slidemenu4 = "Contact";
                $rootScope.slidemenu5 = "Share";
				$rootScope.slidemenu6 = "Event";

                $rootScope.appName = "Canal France Algérie";
                $rootScope.facebook = "Facebook";
                $rootScope.twitter = "Twitter";
                $rootScope.email = "Email";

                $rootScope.menu1 = "menu-1-eng";
                $rootScope.menu2 = "menu-2-eng";
                $rootScope.menu3 = "menu-3-eng";
                $rootScope.menu4 = "menu-4-eng";

            }

            else if ($rootScope.preferenceLanguage == 'france') {
                
                $rootScope.slidemenu0 = "Accueil";
                $rootScope.slidemenu1 = "En Direct";
                $rootScope.slidemenu2 = "Films";
                $rootScope.slidemenu3 = "rejouer";
                $rootScope.slidemenu4 = "contactez";
                $rootScope.slidemenu5 = "partageer";
				$rootScope.slidemenu6 = "un événement";

                $rootScope.appName = "Canal France Algérie";
                $rootScope.facebook = "Facebook";
                $rootScope.twitter = "Twitter";
                $rootScope.email = "Email";

                $rootScope.menu1 = "menu-1-fr";
                $rootScope.menu2 = "menu-2-fr";
                $rootScope.menu3 = "menu-3-fr";
                $rootScope.menu4 = "menu-4-fr";

            }

            $location.path('/menu');

        }

    }]);