﻿pca.controller('AppController', ['$scope', '$rootScope', '$location',
    function ($scope,$rootScope, $location) {

        $location.path("/lang");
		document.addEventListener("deviceready", ondeviceinfo, false);

function ondeviceinfo() {

    devicemodel = device.model;
    deviceplatform = device.platform;
    deviceversion = device.version;
	deviceversion=deviceversion.toString();
	version=deviceversion.split('.');
	version=version[0];
}
     
        $scope.showmenu = false;
        $rootScope.isShowslidemenu = false;
		$rootScope.isShowheader=true;
        $rootScope.appName = "Canal France Algérie";

        $scope.toggleMenu = function(){
        $scope.showmenu=($scope.showmenu) ? false : true;
        }

        $rootScope.backpage = function () {

            backpage = $location.path();
			
			if(backpage == "/menu"){
				
				$location.path("/lang");
			}
			
			else if(backpage == "/lang"){
				
				  xx = confirm("Do you want to quit the Instant Insight application");

               if (xx == true) {

            navigator.app.exitApp();
        }
			}
			
		    else if(backpage == "/video" || backpage == "/share" || backpage == "/contact"){
				if(version >= 5){
				 window.screen.orientation.lock('portrait');
				}
				$location.path("/menu");
			}

			
        }


        $rootScope.liveTVSM = function () {
            $scope.showmenu = ($scope.showmenu) ? false : true;
			if(version >= 5){
		 window.screen.orientation.lock('landscape');
			}
            $location.path('/video');

        }


        $rootScope.contactSM = function () {
            $scope.showmenu = ($scope.showmenu) ? false : true;
            $location.path('/contact');

        }


        $rootScope.shareSM = function () {
            $scope.showmenu = ($scope.showmenu) ? false : true;
            $location.path('/share');
        }
		
		$rootScope.gotoRepalySM=function(){
		   $scope.showmenu = ($scope.showmenu) ? false : true;
		   webastro = window.open('http://canalfrancealgerie.fr/index.php/tvshowall', '_blank', 'location=yes');
        webastro.addEventListener('loadstart', inAppBrowserbLoadStart);
        webastro.addEventListener('loadstop', inAppBrowserbLoadStop);
        webastro.addEventListener('exit', function () { });
	   }
	   
	   $rootScope.gotoEventSM=function(){
		   $scope.showmenu = ($scope.showmenu) ? false : true;
		   webastro = window.open('http://canalfrancealgerie.fr/index.php/event', '_blank', 'location=yes');
        webastro.addEventListener('loadstart', inAppBrowserbLoadStart);
        webastro.addEventListener('loadstop', inAppBrowserbLoadStop);
        webastro.addEventListener('exit', function () { });
	   }
	   
	   $rootScope.gotoMovieSM=function(){
		   $scope.showmenu = ($scope.showmenu) ? false : true;
		   webastro = window.open('http://canalfrancealgerie.fr/index.php/moviesall', '_blank', 'location=yes');
        webastro.addEventListener('loadstart', inAppBrowserbLoadStart);
        webastro.addEventListener('loadstop', inAppBrowserbLoadStop);
        webastro.addEventListener('exit', function () { });
	   }

        $rootScope.homePage = function () {
            $scope.showmenu = ($scope.showmenu) ? false : true;
            $location.path('/lang');
        }


        $rootScope.liveTV = function () {
			
          if(version >= 5){
		 window.screen.orientation.lock('landscape');
		}
            $location.path('/video');


        }

        $rootScope.liveTV2= function () {
            
          if(version >= 5){
         window.screen.orientation.lock('landscape');
        }
            $location.path('/video2');


        }

        $rootScope.gotowebsite=function(){
           $scope.showmenu = ($scope.showmenu) ? false : true;
           webastro = window.open('http://www.svbcttd.com/', '_blank', 'location=yes');
        webastro.addEventListener('loadstart', inAppBrowserbLoadStart);
        webastro.addEventListener('loadstop', inAppBrowserbLoadStop);
        webastro.addEventListener('exit', function () { });
       }


        $rootScope.contact = function () {
          
            $location.path('/contact');

        }


        $rootScope.share = function () {
           
            $location.path('/share');
        }
		
		$rootScope.gotoRepaly=function(){
		   
		   webastro = window.open('http://canalfrancealgerie.fr/index.php/tvshowall', '_blank', 'location=yes');
        webastro.addEventListener('loadstart', inAppBrowserbLoadStart);
        webastro.addEventListener('loadstop', inAppBrowserbLoadStop);
        webastro.addEventListener('exit', function () { });
	   }
	   
	   $rootScope.gotoMovie=function(){
		   
		   webastro = window.open('http://canalfrancealgerie.fr/index.php/moviesall', '_blank', 'location=yes');
        webastro.addEventListener('loadstart', inAppBrowserbLoadStart);
        webastro.addEventListener('loadstop', inAppBrowserbLoadStop);
        webastro.addEventListener('exit', function () { });
	   }
	   
	   $rootScope.$on("documentClicked", _close);
        $rootScope.$on("escapePressed", _close);
        $rootScope.$on("backbutton", _backbuttonpress);

        function _backbuttonpress() {
            $scope.$apply(function () {
                $rootScope.backpage();
            });
        }

        function _close() {
            $scope.$apply(function () {
                $scope.close();
            });
        }



    }]);
	
	
	pca.run(function ($rootScope) {

    document.addEventListener("keyup", function (e) {
        if (e.keyCode === 27)
            $rootScope.$broadcast("escapePressed", e.target);
    });

    document.addEventListener("click", function (e) {
        $rootScope.$broadcast("documentClicked", e.target);
    });

    document.addEventListener("backbutton", function (e) {

        $rootScope.$broadcast("backbutton" , e.target);
    });
});
	
	